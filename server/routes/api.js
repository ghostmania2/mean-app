const express = require('express');
const router = express.Router();
const Survey = require('../schemas/survey');



/* GET api listing. */
router.get('/', (req, res) => {
  res.send('api works');
});

router.get('/surveys', (req,res) => {
  Survey.find(function(err, surveys){
    if (err) res.send(err)
    res.json(surveys);
  })
});

router.post('/surveys', function(req,res){
  var survey = new Survey();
  survey.name = req.body.name;
  survey.text = req.body.text;
  survey.save(function(err){
    if(err) res.send(err)
    res.json({message: 'survey created'})
  })
});

router.delete('/surveys/:survey_id', function(req,res){
  Survey.remove({
    _id: req.params.survey_id
  }, function (err, user) {
    if (err)
        res.send(err);
    res.json({message: 'Survey deleted'});
  });
})

router.put('/surveys/:survey_id', function (req, res) {
  Survey.findById(req.params.survey_id, function (err, survey) {
      if (err)
          res.send(err);
      survey.name = req.body.name;
      survey.text = req.body.text;

      survey.save(function (err) {
          if (err)
              res.send(err);
          res.json({message: 'survey updated'});
      });
  });
});


module.exports = router;
