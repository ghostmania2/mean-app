import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-report',
  templateUrl: './report.component.html',
  styleUrls: ['./report.component.css']
})
export class ReportComponent implements OnInit {

  title = 'Compliance Report';

  constructor() { }

  ngOnInit() {
    console.log('report component')
  }

}
