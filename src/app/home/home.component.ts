import { Component, OnInit } from '@angular/core';
import { SurveyServiceService } from '../survey-service.service';
import { Location } from '@angular/common';
import { Router } from '@angular/router';
import { identifierModuleUrl } from '@angular/compiler';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  constructor(private surveyService: SurveyServiceService,
              private location: Location,
              private router: Router) { }
  title = 'View all Surveys';

  public surveys;
  public currentSurvey = {};
  public data = {};

  ngOnInit() {
    console.log('app component');
    $('.modal').modal(); // to make materialize modals work
    this.getSurveys();
  }

  getSurveys() {
    this.surveyService.getSurveys().subscribe(
      data => {console.log(data); this.surveys = data; },
      err => console.error(err),
      () => console.log('request ended'));
  }

  showSurveyDetails(survey) {
    console.log('current survey', survey);
    this.currentSurvey = survey;
  }

  updateSurvey(name, text) {
    this.data = {
      name: name,
      text: text,
      _id: this.currentSurvey._id
    };
    this.surveyService.updateSurvey(this.data).subscribe(
      data => {console.log(data); this.getSurveys(); this.currentSurvey = {}; this.data = {}; },
      err => console.error(err),
      () => console.log('request ended'));
  }

  removeSurvey(survey) {
    console.log(survey);
    this.surveyService.removeSurvey(survey._id).subscribe(
      data => {console.log(data); this.getSurveys(); },
      err => console.error(err),
      () => console.log('request ended'));
  }

}
