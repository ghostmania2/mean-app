import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable()
export class SurveyServiceService {

  constructor(private http: HttpClient) { }

  createSurvey(survey) {
    return this.http.post('api/surveys', survey);
  }

  getSurveys() {
    return this.http.get('api/surveys');
  }

  removeSurvey(id) {
    return this.http.delete('api/surveys/' + id);
  }

  updateSurvey(data) {
    return this.http.put('api/surveys/' + data._id, data);
  }


}
