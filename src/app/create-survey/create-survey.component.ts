import { Component, OnInit } from '@angular/core';
import { SurveyServiceService } from '../survey-service.service';

@Component({
  selector: 'app-create-survey',
  templateUrl: './create-survey.component.html',
  styleUrls: ['./create-survey.component.css']
})
export class CreateSurveyComponent implements OnInit {

  constructor(private surveyService: SurveyServiceService) { }
  model = {};
  message;
  ngOnInit() {
    console.log('create survey component');
  }

  createSurvey(model, form) {
    // this.model = model;
    this.surveyService.createSurvey(model).subscribe(data => {
      console.log(data);
      this.message = data;
      setTimeout(() => {
        this.message = '';
      }, 3000);
      form.reset();
    });
  }


}
